package com.nubank.injection

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import com.nubank.data.source.ShortenerRepository
import com.nubank.data.source.remote.ShortenerAPI
import com.nubank.data.source.remote.ShortenerRemoteDataSource
import com.nubank.view.ViewModelFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object Injection {
    inline fun <reified T : ViewModel> provideViewModel(activity: AppCompatActivity) =
            ViewModelProviders.of(activity, ViewModelFactory(provideSortenerRepository())).get(T::class.java)

    fun provideSortenerRepository() = ShortenerRepository(ShortenerRemoteDataSource(provideShortnerAPI()))

    fun provideShortnerAPI() : ShortenerAPI {
        val retrofit = Retrofit.Builder()
                .baseUrl(ShortenerAPI.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        return retrofit.create<ShortenerAPI>(ShortenerAPI::class.java)
    }
}