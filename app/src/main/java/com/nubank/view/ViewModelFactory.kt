package com.nubank.view

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.nubank.data.source.ShortenerDataSource
import com.nubank.view.shortener.ShortenerViewModel

class ViewModelFactory(private val repository: ShortenerDataSource) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(ShortenerViewModel::class.java) ->
                        ShortenerViewModel(repository)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

}