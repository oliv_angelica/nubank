package com.nubank.view.shortener

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.nubank.data.Alias
import com.nubank.data.source.ShortenerDataSource
import com.nubank.view.SingleLiveEvent

class ShortenerViewModel(private val repository: ShortenerDataSource) : ViewModel() {

    val searchEvent = SingleLiveEvent<SearchEvent>()
    val uiData = MutableLiveData<List<Alias>>()
    val aliasList = mutableListOf<Alias>()
    val fullUrlData = MutableLiveData<String>()

    fun searchUrl(urlString: String) {
        searchEvent.value = SearchEvent(isLoading = true)

        repository.getShortenerResult(urlString, object : ShortenerDataSource.Callback {
            override fun onSuccess(alias: Alias) {
                aliasList.add(alias)
                uiData.postValue(aliasList)
            }

            override fun onFail() {
                searchEvent.postValue(SearchEvent(isLoading = false, isSuccess = false))
            }

        })
    }
}

data class SearchEvent(val isLoading: Boolean = false, val isSuccess: Boolean = false)