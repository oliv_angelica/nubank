package com.nubank.view.shortener;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nubank.R;
import com.nubank.data.Alias;

import java.util.List;

/**
 * Created by NubankReviewer on 8/4/18.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    private ClickRecyclerViewInterface clickRecyclerViewInterface;
    Context context;
    private List<Alias> list;

    public RecyclerViewAdapter(List<Alias> list, Context context, ClickRecyclerViewInterface clickRecyclerViewInterface1){
        this.context = context;
        this.list = list;
        clickRecyclerViewInterface = clickRecyclerViewInterface1;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder recyclerViewHolder, final int i) {
        Alias alias = list.get(i);

        recyclerViewHolder.tvAlias.setText(alias.getAlias());
        recyclerViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickRecyclerViewInterface.onCustomClick(list.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    protected class RecyclerViewHolder extends RecyclerView.ViewHolder {

        protected TextView tvAlias;

        public RecyclerViewHolder(final View itemView) {
            super(itemView);
            tvAlias = (TextView) itemView.findViewById(R.id.tvAlias);
        }
    }
}
