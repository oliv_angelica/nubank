package com.nubank.view.shortener

import android.arch.lifecycle.Observer
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.nubank.R

import com.nubank.data.Alias
import com.nubank.injection.Injection
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import java.util.ArrayList

/**
 * Created by NubankReviewer on 8/4/18.
 */

class ShortenerActivity : AppCompatActivity(), ClickRecyclerViewInterface {

    private var recyclerAdapter: RecyclerViewAdapter? = null
    private val lista = ArrayList<Alias>()
    private lateinit var viewModel: ShortenerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvLista.setLayoutManager(LinearLayoutManager(this))

        viewModel = Injection.provideViewModel(this)
        viewModel.uiData.observe(this, Observer { aliasList ->
            rvLista.adapter = RecyclerViewAdapter(aliasList, this, this)
        })

        viewModel.fullUrlData.observe(this, Observer { fullUrl ->
            try {
                val uris = Uri.parse(fullUrl)
                val intents = Intent(Intent.ACTION_VIEW, uris)
                val b = Bundle()
                b.putBoolean("new_window", true)
                intents.putExtras(b)
                startActivity(intents)
            } catch (erro: ActivityNotFoundException) {
                Toast.makeText(this, fullUrl, Toast.LENGTH_LONG).show()
            }
        })

        btnEnviaUrl.setOnClickListener{
            viewModel.searchUrl(etUrl.text.toString())
        }
    }

    override fun onCustomClick(alias: Alias) {


    }

}
