package com.nubank.view.shortener;

import com.nubank.data.Alias;

/**
 * Created by NubankReviewer on 8/4/18.
 */

public interface ClickRecyclerViewInterface {
    void onCustomClick(Alias object);
}
