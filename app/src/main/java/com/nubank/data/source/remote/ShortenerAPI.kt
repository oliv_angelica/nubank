package com.nubank.data.source.remote

import com.nubank.data.Alias
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ShortenerAPI {

    @POST("api/alias")
    fun getAliasUrl(@Body urlRequest: UrlRequest): Call<Alias>

    @GET("api/alias/{alias}")
    fun getFullUrl(@Path("alias") alias: String)

    companion object {
        const val baseUrl = "https://swift-shortener.appspot.com"
    }
}