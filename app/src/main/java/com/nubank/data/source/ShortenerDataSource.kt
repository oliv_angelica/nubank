package com.nubank.data.source

import com.nubank.data.Alias

interface ShortenerDataSource {

    fun getShortenerResult(urlString: String, callback: Callback)

    interface Callback {
        fun onSuccess(alias: Alias)

        fun onFail()
    }
}