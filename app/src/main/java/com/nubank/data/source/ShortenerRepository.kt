package com.nubank.data.source


class ShortenerRepository(val shortenerRemoteDataSource: ShortenerDataSource): ShortenerDataSource {
    override fun getShortenerResult(urlString: String, callback: ShortenerDataSource.Callback) {
        shortenerRemoteDataSource.getShortenerResult(urlString, callback)
    }
}