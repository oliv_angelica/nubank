package com.nubank.data.source.remote

import com.nubank.data.Alias
import com.nubank.data.source.ShortenerDataSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShortenerRemoteDataSource(private val api: ShortenerAPI) : ShortenerDataSource {

    override fun getShortenerResult(urlString: String, callback: ShortenerDataSource.Callback) {
        api.getAliasUrl(UrlRequest(urlString)).enqueue(object : Callback<Alias> {
            override fun onFailure(call: Call<Alias>?, t: Throwable?) {
                callback.onFail()
            }

            override fun onResponse(call: Call<Alias>?, response: Response<Alias>?) {
                response?.body()?.let {
                    callback.onSuccess(it)
                }

            }

        })
    }
}